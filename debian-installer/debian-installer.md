=== Info about debian-installer ===

debian-installer is a strange beast. It needs all its components to be
"synchronised", so if we have forked any package in Devuan it is
fundamental to have the package updated to the same devuan-installer
version before risking to (fail to) build debian-installer.

At the moment (20180118) we have forked for ascii the following source
packages, that that are either debian-installer components or are
required by debian-installer as deps:

 - tasksel (OK)
 - netcfg (OK)
 - main-menu (OK)
 - base-installer (OK)
 - apt-setup (OK)
 - colord2 (OK)
 - choose-mirror (OK)
 - pkgsel (OK)
 - net-retriever (OK)
 - dbus (OK)
 - eudev (OK)
 - rootskel-gtk (TO-BUILD)

When all the d-i components and deps are merged, we can issue a 
d-i build

The logo shown in the GTK installer is configured in the package
"rootskel-gtk". 
