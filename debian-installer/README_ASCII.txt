#### README -- Hacking debian-installer for ascii
(@parazyd, @KatolaZ, 20180117)

The stock debian-installer from stretch does not work due to 
a broken dep in libdebian-installer4

==== third attempt ====

The problem might be in the fact that all packages that go into 
debian-installer (i.e., all the packages used in the generate
isos) need to be "compatible". Now we have indeed forked two 
of those packages, namely libdbus (srcpkg dbus) and libcolord2 
(srcpkg colord). Libdbus has been updated to stretch version, but
libcolord2 is not. 

PLAN:

 - Update libcolord2 to stretch version
 - Conquer the world



==== second attempt: failed ====

We fork at 0.110, trying t fixup deps problems
 
   @@@@@



 ==== first attempt: didn;t work ====
We basically need to revert a change in libdebian-installer4
that has shadowed a symbol. 

This is what we did

- git clone https://anonscm.debian.org/cgit/d-i/libdebian-installer.git

- git checkout 0.110

  0.110 is the version built in stretch, which is broken for us

- git revert abd6501c35c658fd5f90997c0e1bb5d608c1c991

  ^^^^ this is thebloody commit -- we are reverting it

- git checkout -b suites/ascii

- adjusted conflict in changelog

- added new changelog entry (version 0.110+0.109+devuan1)


Then we pushed and tagged

Actually, some of the changes needed to be there. So we proceeded
by re-adding the stuff, one symbol at a time...


PROCEDURE:

- 


