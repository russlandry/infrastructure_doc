Installing a ganeti node for devuan on a soyoustart dedicated server.

(rrq) NOTE: This is out of date at 2020-06-22.

Preinstall - add the new server details into the ganeti-nodes.rst including the
    the next IP addresses for lan_br, dmz_br and gnt_br

1: Initial OS install
Select the template gear icon and select "Personal Templates"
Select the template "Wheezy with Optimal LVM (64bit) - should be default

Note:

- this template give us a reasonable LVM setup to work with.
- if you want to change the template you need to do that in "Templates" from the server control panel.

2: Upgrade to Devuan - currently Jessie and add backports repo.

3: apt-install bridge-utils

4: Setup the network config as per the ./network_interfaces file provided

5: Install from backports ganeti and the latest backports kernel (currently 4.9 in jessie-backports).

6: get the openvpn up and running - see the provided openvpn stanza's

7: install ganeti-ovh hooks and configure 

8: add the node to our ganeti cluster

9: spin up a test vm using our fancy ganeti interface.. (doesn't exist yet...)

