#!/bin/sh

##
## Split the mirror_list.txt file and create a separete file for each 
## mirror with the corresponding  RFC822 stanza 
##

cat ${HOME}/mirror_list.txt | grep -v "^#" | grep -v "^\+" | awk -v RS= '{print > ("mirrors/mirror-" NR ".txt")}'
