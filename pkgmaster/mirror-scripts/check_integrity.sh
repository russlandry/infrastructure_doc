#!/bin/sh

##
## Check the integrity of a mirror by verifying that the Release
## files are signed with the Devuan gpg key
##
##


if [ $# -lt 1 ]; then 
	echo "Usage: $0 <BaseURL> [<as-host>]"
	exit 1
fi


if  [ $# -gt 1 ]; then 
	AS_HOST="--header \"Host: $2\""
fi	

URL="$1"

LOCAL_REPO="/home/vesta/devuan"
REF_DIRS="devuan/dists merged/dists"

CHECK_SIG="gpg --no-default-keyring --keyring /usr/share/keyrings/devuan-archive-keyring.gpg --verify"

for rd in ${REF_DIRS}; do 
	for d in $(ls ${LOCAL_REPO}/$rd); do	
		## avoid checks on symlinks
		if [ ! -h "${LOCAL_REPO}/$rd/$d" ]; then 
			tmp1=$(tempfile)
			tmp2=$(tempfile)
			tmp3=$(tempfile)
			##echo "command: curl -s ${AS_HOST} ${URL}/$rd/$d/Release > $tmp1"
			eval curl -s ${AS_HOST} ${URL}/$rd/$d/Release > $tmp1
			eval curl -s ${AS_HOST} ${URL}/$rd/$d/Release.gpg > $tmp2
			eval curl -s ${AS_HOST} ${URL}/$rd/$d/InRelease > $tmp3
			${CHECK_SIG} $tmp2 $tmp1 2>/dev/null
			ret1=$?
			${CHECK_SIG} $tmp3 2>/dev/null
			ret2=$?
			if [ $ret1 -ne 0 ]; then
				NUM_ERR=$((${NUM_ERR} + 1))
				OFFENDING="$OFFENDING /$rd/$d/Release"
			fi
			if [ $ret2 -ne 0 ]; then 
				NUM_ERR=$((${NUM_ERR} + 1))
				OFFENDING="$OFFENDING /$rd/$d/InRelease"
			fi	
			rm $tmp1 $tmp2 $tmp3	
		fi
	done 
done

if [ -n "$OFFENDING" ]; then 
	echo "Offending files:"
	echo $OFFENDING | sed -r -e 's/\ /\n/g'
	echo
fi

exit ${NUM_ERR}
