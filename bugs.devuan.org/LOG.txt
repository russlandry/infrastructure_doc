-- LOG of stuff done for the installation of debbugs --

- We are going to use msmtp as a MTA, so:
  - apt-get install msmtp
- (OBSOLETE) the damn debbugs depends on mail-transport-agent but msmtp does NOT
  Provide mail-transport-agent. Hence, I installed nullmailer
  - apt-get install nullmailer

  There is no doubt that many Debian developers should be beaten with a large
  bat, possibly decorated with an adequate number of rusty nails...
- (OBSOLETE) Since Debian developers should be beaten with a large bat, we need now to 
  disable nullmailer:
  - update-rc.d nullmailer disable 2 3 4 5
  - insserv -v nullmailer,stop=0,1,6
- (OBSOLETE) now, finally, we can install debbugfs without other silly dependencies:
  - apt-get install debbugs

- I made a backup of the original debbugs conf file:
  cp /etc/debbugs/config /etc/debbugs/config.bak

- then, I edited the config file, setting bugs.devuan.org as the debbugs machine

- added RCS dir in /etc/debbugs and in /etc/debbugs/html

- we added the user debbugs@fulcanelli, which will manage bugs emails

- we are finally using msmtp-mta to send emails from the debbugs user 
  through dyne.org. nullmailer has been uninstalled.

- debbugs@fulcanelli uses fetchmail to get emails from devuanbugs@dyne.org

- we installed letsencrypt on fulcanelli, to get automatic certs for 
  bugs.devuan.org. The certbot installation is in /root/certbot. 
  certbot is run by cron to check for updates

- there are a few useful scripts in the folder /home/debbugs/scripts. 
  Please refer to the README in that folder for more information.



